CREATE TABLE `age_groups` (
                              `ID` int NOT NULL AUTO_INCREMENT,
                              `NAME` varchar(45) DEFAULT NULL,
                              `RANGE` varchar(45) DEFAULT NULL,
                              PRIMARY KEY (`ID`),
                              KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `category` (
                            `ID` int NOT NULL,
                            `NAME` varchar(45) NOT NULL,
                            PRIMARY KEY (`ID`),
                            KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `game_type` (
                             `ID` int NOT NULL AUTO_INCREMENT,
                             `TYPE` varchar(45) DEFAULT NULL,
                             PRIMARY KEY (`ID`),
                             KEY `TYPE` (`TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `games` (
                         `GAME_ID` int NOT NULL AUTO_INCREMENT,
                         `NAME` varchar(45) NOT NULL,
                         `TYPE` varchar(45) DEFAULT NULL,
                         `AGE_GRP_NM` varchar(45) DEFAULT NULL,
                         `CATGRY_NM` varchar(45) DEFAULT NULL,
                         `CRE_ID` varchar(10) DEFAULT NULL,
                         `CRE_TS` datetime DEFAULT NULL,
                         `UPDT_ID` varchar(10) DEFAULT NULL,
                         `UPDT_TS` datetime DEFAULT NULL,
                         PRIMARY KEY (`GAME_ID`,`NAME`),
                         UNIQUE KEY `NAME_UNIQUE` (`NAME`),
                         KEY `AGE_GRP_NM` (`AGE_GRP_NM`),
                         KEY `CATGRY_NM` (`CATGRY_NM`),
                         KEY `TYPE` (`TYPE`),
                         CONSTRAINT `games_ibfk_1` FOREIGN KEY (`AGE_GRP_NM`) REFERENCES `age_groups` (`NAME`),
                         CONSTRAINT `games_ibfk_2` FOREIGN KEY (`CATGRY_NM`) REFERENCES `category` (`NAME`),
                         CONSTRAINT `games_ibfk_3` FOREIGN KEY (`TYPE`) REFERENCES `game_type` (`TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `levels` (
                          `ID` int NOT NULL AUTO_INCREMENT,
                          `NAME` varchar(45) DEFAULT NULL,
                          PRIMARY KEY (`ID`),
                          KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `level_category_map` (
                                      `ID` int NOT NULL AUTO_INCREMENT,
                                      `LVL_NM` varchar(45) NOT NULL,
                                      `CAT_ID` int NOT NULL,
                                      `AGE_GRP_NM` varchar(45) NOT NULL,
                                      PRIMARY KEY (`ID`,`LVL_NM`,`CAT_ID`,`AGE_GRP_NM`),
                                      KEY `LVL_FK_idx` (`LVL_NM`),
                                      KEY `CAT_FK_idx` (`CAT_ID`),
                                      KEY `AGE_GRP_FK_idx` (`AGE_GRP_NM`),
                                      CONSTRAINT `AGE_GRP_FK` FOREIGN KEY (`AGE_GRP_NM`) REFERENCES `age_groups` (`NAME`),
                                      CONSTRAINT `CAT_FK` FOREIGN KEY (`CAT_ID`) REFERENCES `category` (`ID`),
                                      CONSTRAINT `LVL_FK` FOREIGN KEY (`LVL_NM`) REFERENCES `levels` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

