CREATE TABLE `learning_curve_schema`.`falling_bubbles` (
                                                           `GAME_ID` INT NOT NULL,
                                                           `NAME` VARCHAR(45) NULL,
                                                           `CRE_TS` DATETIME NULL,
                                                           `CRE_ID` VARCHAR(10) NULL,
                                                           `UPDT_TS` DATETIME NULL,
                                                           `UPDT_ID` VARCHAR(10) NULL,
                                                           PRIMARY KEY (`GAME_ID`));

CREATE TABLE `fb_questions` (
                                `Q_ID` int NOT NULL AUTO_INCREMENT,
                                `GAME_ID` int NOT NULL,
                                `QUESTION` varchar(500) DEFAULT NULL,
                                `CRE_TS` datetime NOT NULL,
                                `CRE_ID` varchar(10) NOT NULL,
                                `UPDT_TS` datetime DEFAULT NULL,
                                `UPDT_ID` varchar(10) DEFAULT NULL,
                                PRIMARY KEY (`Q_ID`,`GAME_ID`),
                                KEY `FK_GAME_idx` (`GAME_ID`),
                                CONSTRAINT `FK_GAME` FOREIGN KEY (`GAME_ID`) REFERENCES `falling_bubbles` (`GAME_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




CREATE TABLE `fb_options` (
                              `OP_ID` int NOT NULL AUTO_INCREMENT,
                              `GAME_ID` int NOT NULL,
                              `Q_ID` int NOT NULL,
                              `OPTION` varchar(500) DEFAULT NULL,
                              `CRE_TS` datetime NOT NULL,
                              `CRE_ID` varchar(10) NOT NULL,
                              `UPDT_TS` datetime DEFAULT NULL,
                              `UPDT_ID` varchar(10) DEFAULT NULL,
                              PRIMARY KEY (`OP_ID`,`GAME_ID`,`Q_ID`),
                              KEY `FK_QUES_idx` (`GAME_ID`,`Q_ID`),
                              CONSTRAINT `FK_QUES` FOREIGN KEY (`GAME_ID`, `Q_ID`) REFERENCES `fb_questions` (`GAME_ID`, `Q_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `fb_answers` (
                              `GAME_ID` int NOT NULL,
                              `Q_ID` int NOT NULL,
                              `OP_ID` int NOT NULL,
                              `CRE_ID` varchar(10) DEFAULT NULL,
                              `CRE_TS` datetime DEFAULT NULL,
                              PRIMARY KEY (`GAME_ID`,`Q_ID`,`OP_ID`),
                              CONSTRAINT `FK_OPT` FOREIGN KEY (`GAME_ID`, `Q_ID`, `OP_ID`) REFERENCES `fb_options` (`GAME_ID`, `Q_ID`, `OP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



